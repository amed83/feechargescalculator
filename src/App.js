import React, { Component } from "react";
import Dashboard from "./components/Dashboard/Dashboard";
import { StyledAppContainer } from "./AppStyles";

class App extends Component {
  render() {
    return (
      <StyledAppContainer>
        <Dashboard />
      </StyledAppContainer>
    );
  }
}

export default App;
