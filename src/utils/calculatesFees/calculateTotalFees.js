import { TIME_FRAME, FEE_PERCENTAGE } from "../../constants/constants";

const calculateTotalFees = (
  startingContribution,
  monthlyContribution,
  months = TIME_FRAME
) => {
  let pot = startingContribution;
  let total = 0;
  for (let i = 1; i < months; i++) {
    pot += monthlyContribution;
    let charges = pot * FEE_PERCENTAGE;
    total += charges;
    pot = pot - charges;
  }
  return total;
};

export default calculateTotalFees;
