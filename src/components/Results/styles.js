import styled from "styled-components";

export const TextContainer = styled.div`
  padding: 1rem 1rem;
  position: absolute;
  top: 58%;
  & strong {
    font-weight: bold;
  }
  @media (max-width: 700px) {
    padding: 0 1rem;
  }
`;
