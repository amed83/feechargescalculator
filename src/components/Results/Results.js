import React from "react";
import { TextContainer } from "./styles";

const Results = ({
  startingContribution,
  monthlyContribution,
  totalFees,
  months
}) => {
  return (
    <TextContainer>
      <p>
        With a starting contribution of{" "}
        <strong> £{startingContribution}</strong> and a monthly contribution of{" "}
        <strong> £{monthlyContribution}</strong> we will deduct a total of{" "}
        <strong> £{totalFees}</strong> in a timeframe of months{" "}
        <strong>{months}</strong>
      </p>
    </TextContainer>
  );
};

export default Results;
