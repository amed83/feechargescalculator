import React, { useState } from "react";
import Form from "../Form/Form";
import { ContributionsForm } from "../Form/formConfig/config";
import {
  StyledFormContainer,
  StyledMainContainer,
  StyledFormTitle,
  StyledTitle
} from "./styles";

import { TIME_FRAME } from "../../constants/constants";

import calculateTotalFees from "../../utils/calculatesFees/calculateTotalFees";
import Results from "../Results/Results";

const Dashboard = () => {
  const [startingContribution, setStartingContribution] = useState(0);
  const [monthlyContribution, setMonthlyContribution] = useState(0);
  const [calculated, setCalculated] = useState(false);
  const [totalFees, setTotalFees] = useState(0);

  const handleSubmit = values => {
    const { starting_contribution, monthly_contribution } = values;

    setStartingContribution(starting_contribution);
    setMonthlyContribution(monthly_contribution);

    const totalFees = calculateTotalFees(
      Number(starting_contribution),
      Number(monthly_contribution)
    ).toFixed(2);

    setCalculated(true);
    setTotalFees(Number(totalFees));
  };

  return (
    <StyledMainContainer>
      <StyledTitle>Fee Charges</StyledTitle>
      <StyledFormContainer>
        <StyledFormTitle>Set your Amounts</StyledFormTitle>
        <Form {...ContributionsForm} handleSubmit={handleSubmit} />
        {calculated && (
          <Results
            startingContribution={startingContribution}
            monthlyContribution={monthlyContribution}
            totalFees={totalFees}
            months={TIME_FRAME}
          />
        )}
      </StyledFormContainer>
    </StyledMainContainer>
  );
};

export default Dashboard;
