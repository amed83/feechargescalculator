import styled from "styled-components";

export const StyledMainContainer = styled.div`
  width: 90%;
  margin-top: 5rem;
  position: relative;
`;

export const StyledFormContainer = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  background-color: white;
  padding: 2rem;
  box-shadow: 0 4px 18px -6px rgba(0, 0, 0, 0.75);
  border: solid 1px #b3b3b3;
  border-radius: 5px;
`;

export const StyledFormTitle = styled.h3`
  margin-left: 1rem;
`;

export const StyledTitle = styled.h2`
  margin-bottom: 3rem;
`;
