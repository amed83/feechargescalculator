export const ContributionsForm = {
  submitText: "Calculate",
  fields: [
    {
      name: "starting_contribution",
      placeholder: "Starting Contribution",
      type: "text",
      validation: {
        required: true
      }
    },
    {
      name: "monthly_contribution",
      placeholder: "Monthly Contribution",
      type: "text",
      validation: {
        required: true
      }
    }
  ]
};
