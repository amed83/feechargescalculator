import { withFormik } from "formik";
import FormConstructor from "./FormConstructor/FormConstructor";

const Form = withFormik({
  mapPropsToValues: ({ fields }) => {
    const values = {};
    fields.forEach(field => {
      values[field.name] = field.value ? field.value : "";
    });
    return values;
  },

  validate: (values, props) => {
    const arrayValues = Object.keys(values);
    const errors = {};
    arrayValues.forEach(value => {
      let findProp = props.fields.find(prop => prop.name === value);
      if (findProp.validation && !values[value]) {
        errors[value] = "Value Required";
      }
    });

    return errors;
  },

  handleSubmit: async (values, { setSubmitting, props, resetForm }) => {
    try {
      await props.handleSubmit(values);
      setSubmitting(false);
    } catch (err) {
      return err;
    }
  }
})(FormConstructor);

export default Form;
