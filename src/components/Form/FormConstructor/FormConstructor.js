import React from "react";
import {
  StyledFieldsContainer,
  StyledField,
  StyledInnerFormContainer,
  StyledButton,
  StyledSingleFieldContainer
} from "../styles";
import { Form, ErrorMessage } from "formik";

const FormConstructor = props => {
  const { fields, submitText, errors } = props;
  const renderFields = () => {
    return (
      fields &&
      fields.map(field => {
        return (
          <StyledSingleFieldContainer key={field.name}>
            <span> £</span>
            <p> {field.placeholder} </p>
            <StyledField type={field.type} name={field.name} />
            {errors && <ErrorMessage name={field.name} component="p" />}
          </StyledSingleFieldContainer>
        );
      })
    );
  };

  const { handleSubmit } = props;
  return (
    <Form onSubmit={handleSubmit}>
      <StyledInnerFormContainer>
        <StyledFieldsContainer>{renderFields()}</StyledFieldsContainer>
        <StyledButton type="submit">{submitText}</StyledButton>
      </StyledInnerFormContainer>
    </Form>
  );
};

export default FormConstructor;
