import FormConstructor from "./FormConstructor";

describe("FormConstructor", () => {
  let wrapper;
  let props;
  let handleSubmitMock;

  beforeEach(() => {
    handleSubmitMock = jest.fn();
    props = {
      handleSubmit: handleSubmitMock
    };

    wrapper = shallow(<FormConstructor {...props} />);
  });

  test("the component should render correctly ", () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });

  test("when the form is submitted the handleSubmit function should be called ", () => {
    const form = wrapper.find("Form");
    form.simulate("submit");
    expect(handleSubmitMock.mock.calls.length).toBe(1);
  });
});
