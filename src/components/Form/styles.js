import styled from "styled-components";
import { Field } from "formik";

export const StyledFieldsContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  padding: 2rem 0 3rem;
  width: 100%;
  & div {
    width: 48%;
  }
  & p {
    font-weight: bold;
    margin-bottom: 2px;
  }
`;

export const StyledInnerFormContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const StyledField = styled(Field)`
  width: 90%;
  padding: 0.5rem 1rem;
  font-size: 18px;
  border-top: none;
  border-right: none;
  border-left: none;
  border-bottom: solid 0.5px #757575;
`;

export const StyledButton = styled.button`
  background-color: #2fb153;
  padding: 0.8rem 1.8rem;
  color: #ffff;
  border-radius: 0.2rem;
  align-self: self-start;
  margin-left: 1rem;
  font-size: 16px;
  margin-bottom: 2rem;
  margin-top: 1rem;
  @media (max-width: 700px) {
    margin-top: 3rem;
  }
`;

export const StyledSingleFieldContainer = styled.div`
  position: relative;

  > span {
    position: absolute;
    top: 50%;
    @media (max-width: 500px) {
      top: 63%;
    }
  }
`;
