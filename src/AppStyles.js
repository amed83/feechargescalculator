import styled from "styled-components";

export const StyledAppContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 1rem;
  background-color: #f4f5f4;
`;
